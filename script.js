fetch('https://jsonplaceholder.typicode.com/todos')
.then ((response) => response.json())
.then ((data) => {
	let list = data.map((todo)=>{
		return todo.title;
	});
	console.log(list)
})

//let list = data.map();

fetch('https://jsonplaceholder.typicode.com/todos/22')
.then ((response) => response.json())
.then ((data) => console.log(`The item ${data.title} has a status of ${data.completed}`))

fetch ('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  headers:{
    'Content-type':'application/json'
  },

  

  body: JSON.stringify({
      title: 'Created a to do list item',
      completed: false,
      userID: 1
  })
})

.then ((response) => response.json())
.then((data) => console.log (data));


fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method:'PUT',
	headers:{
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
	  	description: 'To update the my to do list with a different data structure.',
	  	status: 'Pending',
	  	dateCompleted: 'Pending',
	  	userId: 1
	})
})
.then(response => response.json())
.then(data => console.log(data));

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: "PATCH",
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: 'Complete',
	  	dateCompleted: '01/19/22'
	})
})
.then((response)=> response.json())
.then((data)=> console.log(data));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then((response) => response.json())
.then ((data) => console.log(data))
